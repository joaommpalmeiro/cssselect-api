# cssselect-api

> https://cssselect-api.onrender.com + https://cssselect-api.onrender.com/docs

| Field         | Value                                          |
| ------------- | ---------------------------------------------- |
| Environment   | Python 3                                       |
| Build Command | `pip install -r requirements.txt`              |
| Start Command | `uvicorn main:app --host 0.0.0.0 --port 10000` |

## Development

### First time

- `python3 -m venv env`
- `source env/bin/activate`
- `which python`
- `python3 -m pip install -r requirements.txt`
- `python3 -m pip install -r requirements-dev.txt`
- `uvicorn main:app --host 0.0.0.0 --port 10000 --reload`
- `isort --profile black main.py && black main.py`
- `deactivate`

### Remaining times

- `source env/bin/activate`
- `uvicorn main:app --host 0.0.0.0 --port 10000 --reload`
- `isort --profile black main.py && black main.py`
- `deactivate`

## References

- https://github.com/render-examples/fastapi
- https://render.com/docs/environment-variables#python-3
- https://github.com/render-examples/flask-hello-world + https://render.com/docs/deploy-flask
- https://cssselect.readthedocs.io/en/latest/
- https://ysk24ok.github.io/2021/09/02/difference_between_def_and_async_def_in_fastapi.html

## Notes

- https://render.com/docs/free:
  - "(...) only one free database can be active at a time."
- https://render.com/docs/python-version
- `rm -rf env/`
- https://www.cookiecutter.io/
