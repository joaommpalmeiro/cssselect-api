from cssselect import HTMLTranslator, SelectorSyntaxError
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, StrictStr

BAD_REQUEST_MSG: str = "Invalid CSS selector."


class InputPattern(BaseModel):
    # css_selector: str
    css_selector: StrictStr

    class Config:
        schema_extra = {
            "example": {
                "css_selector": "table.dataframe",
            }
        }


class OutputPattern(BaseModel):
    xpath_expression: str


# https://fastapi.tiangolo.com/tutorial/metadata/#metadata-for-api
# https://fastapi.tiangolo.com/tutorial/metadata/#openapi-url
# https://fastapi.tiangolo.com/tutorial/metadata/#docs-urls
app = FastAPI(title="cssselect-api", redoc_url=None)

# https://fastapi.tiangolo.com/tutorial/cors/
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

# https://fastapi.tiangolo.com/async/#in-a-hurry
# https://www.uvicorn.org/
# http://0.0.0.0:10000
# http://0.0.0.0:10000/docs
# https://fastapi.tiangolo.com/tutorial/body/
# https://docs.pydantic.dev/
# https://docs.pydantic.dev/usage/types/
# https://docs.pydantic.dev/usage/schema/ (`Field`)
# https://fastapi.tiangolo.com/tutorial/handling-errors/#use-httpexception
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
# https://cssselect.readthedocs.io/en/latest/#cssselect.SelectorSyntaxError
# https://fastapi.tiangolo.com/tutorial/response-model/
# https://fastapi.tiangolo.com/tutorial/schema-extra-example/
# https://fastapi.tiangolo.com/advanced/additional-responses/
# "[QUESTION] Include possible HTTPExceptions in OpenAPI spec":
# - https://github.com/tiangolo/fastapi/issues/1999
# - https://github.com/tiangolo/fastapi/issues/1999#issuecomment-685365512
# - https://github.com/tiangolo/fastapi/issues/1999#issuecomment-735639995 +
# - https://fastapi.tiangolo.com/tutorial/handling-errors/?h=#the-resulting-response +
# - https://fastapi.tiangolo.com/advanced/additional-responses/#additional-response-with-model
# https://github.com/Kludex/fastapi-responses
# https://docs.pydantic.dev/usage/types/#strict-types


@app.post(
    "/",
    response_model=OutputPattern,
    responses={
        400: {
            "content": {"application/json": {"example": {"detail": BAD_REQUEST_MSG}}},
        },
    },
)
# async def root(pattern: InputPattern):
def root(pattern: InputPattern):
    try:
        xpath_expression = HTMLTranslator().css_to_xpath(pattern.css_selector)
    except SelectorSyntaxError:
        raise HTTPException(status_code=400, detail=BAD_REQUEST_MSG)

    return {"xpath_expression": xpath_expression}
